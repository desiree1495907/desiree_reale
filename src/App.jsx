import { useState } from "react";

import Navbar from "./components/Navbar/Navbar";
import Carousel from "./components/Carousel/Carousel";
import Brand from "./components/Brand/Brand";
import Card from "./components/Card/Card";
import TourDates from "./components/TourDates/TourDates";
import Contacts from "./components/Contact/Contact";
import Footer from "./components/Footer/Footer";

import '@fortawesome/fontawesome-free/css/all.min.css';


import "./App.css";

function App() {
  return (
    <div>
      <Navbar />
      <Carousel />
      <Brand />

      {/* Ho i link qui, perchè li avevo passati come props all'interno di Card*/}
      <main className="container">
        <div className="row">
          {/* Card 1 */}
          <Card
            imageUrl="https://images.unsplash.com/photo-1593844310233-ab7961f9f685?q=80&w=1374&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
            title="Some example shot number one of the card."
            linkUrl="https://images.unsplash.com/photo-1593844310233-ab7961f9f685?q=80&w=1374&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
          />
          {/* Card 2 */}
          <Card
            imageUrl="https://images.unsplash.com/photo-1534270804882-6b5048b1c1fc?q=80&w=1306&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
            title="Some example shot number two of the card."
            linkUrl="https://images.unsplash.com/photo-1534270804882-6b5048b1c1fc?q=80&w=1306&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
          />
          {/* Card 3 */}
          <Card
            imageUrl="https://images.unsplash.com/photo-1533927361641-06ec97c01dfe?q=80&w=1364&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
            title="Some example shot number three of the card."
            linkUrl="https://images.unsplash.com/photo-1533927361641-06ec97c01dfe?q=80&w=1364&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
          />
        </div>
      </main>

      <TourDates />
    
      <Contacts />
      <hr />
      <Footer />
    </div>
  );
}

export default App;
