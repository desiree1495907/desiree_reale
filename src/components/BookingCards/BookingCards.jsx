function BookingCards() {
  return (

    <div className="container">
      <h2 className="text-center">Booking</h2>
      <div className="row">
        <div className="col-md-4">
          <div className="card september">
            <img className="card-img-top" src="https://st3.depositphotos.com/3129191/17589/i/450/depositphotos_175894364-stock-photo-big-oak-kensington-park.jpg" alt="Immagine Settembre" width="300" height="200" />
            <div className="card-body">
              <h5 className="card-title">September</h5>
              <p className="card-text">Book your stay for September!</p>
              <a href="#" className="btn btn-primary">Book</a>
            </div>
          </div>
        </div>
        <div className="col-md-4">
          <div className="card october">
            <img className="card-img-top" src="https://static3.depositphotos.com/1000438/105/i/450/depositphotos_1058020-stock-photo-flock-of-wild-geese-in.jpg" alt="Immagine Ottobre" width="300" height="200" />
            <div className="card-body">
              <h5 className="card-title">October</h5>
              <p className="card-text">Book your stay for October!</p>
              <a href="#" className="btn btn-primary">Book</a>
            </div>
          </div>
        </div>
        <div className="col-md-4">
          <div className="card november">
            <img className="card-img-top" src="https://st.depositphotos.com/2117021/4260/i/450/depositphotos_42606223-stock-photo-bench-on-autum.jpg" alt="Immagine Novembre" width="300" height="200" />
            <div className="card-body">
              <h5 className="card-title">November</h5>
              <p className="card-text">Book your stay for November!</p>
              <a href="#" className="btn btn-primary">Book</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default BookingCards;
