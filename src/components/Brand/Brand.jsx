function Brand(){
    return(
            <div className="container brand">
                <h1 className="text-center">Titolo del Brand</h1>
                <h2 className="text-center">Sottotitolo</h2>
                <section className="text-center">
                    <p>Contenuto centrale</p>
                </section>
            </div>
        );
    }

export default Brand;