
//passo i link delle foto come props al componente <Card>:
function Card({ imageUrl, title, linkUrl }) {
  return (
    
    <div className="col-md-4">
      <div className="card">
        <img className="card-img-top" src={imageUrl} alt="Card image cap" />
        <div className="card-body">
          <p className="card-text">{title}</p>
          <a href={linkUrl} className="card-link">Photo link</a>
        </div>
      </div>
    </div>
  );
}

export default Card;
