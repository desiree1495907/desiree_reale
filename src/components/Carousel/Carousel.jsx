function Carousel(){
    
    return(
        <header>
        <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
            <ol className="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div className="carousel-inner">
                <div className="carousel-item active">
                    <img className="d-block w-100" src="https://images.unsplash.com/photo-1709065556197-2cbe782878e1?q=80&w=2670&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" alt="First slide" />
                    <div className="carousel-caption d-none d-md-block">
                        <h5>New York</h5>
                        <p>uno</p>
                    </div>
                </div>
                <div className="carousel-item">
                    <img className="d-block w-100" src="https://images.unsplash.com/photo-1706661849307-9f0ff8155bc9?q=80&w=1170&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" alt="Second slide" />
                    <div className="carousel-caption d-none d-md-block">
                        <h5>New York</h5>
                        <p>due</p>
                    </div>
                </div>
                <div className="carousel-item">
                    <img className="d-block w-100" src="https://images.unsplash.com/photo-1523374228107-6e44bd2b524e?q=80&w=1170&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" alt="Third slide" />
                    <div className="carousel-caption d-none d-md-block">
                        <h5>New York</h5>
                        <p>tre</p>
                    </div>
                </div>
            </div>
            <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="sr-only">Previous</span>
            </a>
            <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="sr-only">Next</span>
            </a>
        </div>
    </header>
);
}

export default Carousel;
