import ContactForm from "../ContactForm/ContactForm";

function Contacts() {
    return (
      <div className="container mt-5">
        <h2 className="text-center">Contact</h2>
        <p className="text-center">We love our fans!</p>
        <div className="row align-items-center justify-content-center">
        <div className="col-md-6 my-4 text-left">
            <p><i className="fas fa-map-marker-alt"></i> Chicago, US</p>
            <p><i className="fas fa-phone"></i> Phone: +00 1515151515</p>
            <p><i className="fas fa-envelope"></i> Email: email@prova.com</p>
          </div>
          {/* Form */}
          <div className="col-md-6 my-4">
            <ContactForm />
          </div>
        </div>
      </div>
    );
  }
  
  export default Contacts;