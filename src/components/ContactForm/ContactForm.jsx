function ContactForm() {
    return (
      <form>
        <div className="row">
          <div className="col-md-6">
            <div className="form-group">
              <input type="text" className="form-control" placeholder="Name" />
            </div>
          </div>
          <div className="col-md-6">
            <div className="form-group">
              <input type="text" className="form-control" placeholder="Email" />
            </div>
          </div>
        </div>
        <div className="form-group">
          <textarea className="form-control" rows="3" placeholder="Comment"></textarea>
        </div>
        <button type="submit" className="btn btn-primary btn-block">Invia</button>
      </form>
    );
  }
  
  export default ContactForm;