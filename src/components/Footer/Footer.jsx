function Footer() {
    return (
      <footer>
        <div className="container">
          <div className="row">
            <div className="col text-center">
              <h4>From the Blog</h4>
            </div>
          </div>
          <div className="row">
            <div className="col text-left">
              <h2>Mike Ross, Manager</h2>
              <p>Man who loves to travel</p>
            </div>
          </div>
        </div>
      </footer>
    );
  }
  
  export default Footer;