
import BookingCards from "../BookingCards/BookingCards";

function TourDates() {
  return (
    
    <section className="tour-dates">
      <div className="container">
        <h2 className="text-center">TOUR DATES</h2>
        <p className="text-center">Here you will find the dates for the tour</p>
        <p className="text-center">Remember to book your ticket!</p>

        <div className="tour-table">
          <table className="table">
            <thead>
              <tr>
                <th className="text-left" scope="col">MONTH</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className="text-left">September <span>Sold Out !</span></td>
              </tr>
              <tr>
                <td className="text-left">October <span>Sold Out !</span></td>
              </tr>
              <tr>
                <td className="text-left">November</td>
              </tr>
            </tbody>
          </table>
        </div>
        <BookingCards />
      </div>
    </section>
  );
}

export default TourDates;
